// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCoBEk28lM4Pm8Mh5eGnMH2mxHrWGcLE3w",
    authDomain: "e-contact-z.firebaseapp.com",
    projectId: "e-contact-z",
    storageBucket: "e-contact-z.appspot.com",
    messagingSenderId: "190440369118",
    appId: "1:190440369118:web:c20d91b06401d5b64f8b4f",
    measurementId: "G-78X4YB6MN5"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
