import { Component } from '@angular/core';
import { CONTACT, ContactService } from '../services/contact.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  img: any = "assets/images/user.png"
  contacts: any

  constructor(
    public ct: ContactService
  ) {

  }

  ngOnInit() {
    this.ct.get_Contact_Favorite().subscribe((res) => {
      this.contacts = res.map((t) => {
        return {
          id: t.payload.doc.id,
          ...t.payload.doc.data() as CONTACT
        };
      })

      console.log(this.contacts)
    });
  }

}
