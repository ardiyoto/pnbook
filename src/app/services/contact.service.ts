import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';

export interface CONTACT {
  $key: string,
  name: string,
  phone: string,
  email: string,
  avatar: any,
  fav: any
}

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(
    public fStore: AngularFirestore,
    public fBase: AngularFireDatabase,
    public router: Router
  ) { }

  add_Contact(item: CONTACT) {
    return this.fStore.collection("contacts").add(item)
  }

  get_Contacts() {
    return this.fStore.collection("contacts").snapshotChanges()
  }

  get_Contact(id) {
    return this.fStore.collection("contacts").doc(id).valueChanges()
  }

  update_Contact(id, item: CONTACT) {
    return this.fStore.collection('contacts').doc(id).update(item)
  }

  delete_Contact(id: string) {
    return this.fStore.doc('contacts/' + id).delete()
  }

  get_Contact_Favorite() {
    return this.fStore.collection("contacts", ref => ref.where("fav", "==", 1)).snapshotChanges()
  }
}
