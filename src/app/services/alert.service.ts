import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    public toast: ToastController,
    public loading: LoadingController
  ) { }

  async notif(text: any, type: any, timeout: any = 1000) {
    const toast = await this.toast.create({
      message: text,
      duration: timeout,
      color: type
    });
    toast.present();
  }

  show_loading() {
    this.loading.create({
      message: 'Please wait...',
    }).then(response => {
      response.present()
    })
  }

  hide_loading() {
    this.loading.dismiss()
  }
}
