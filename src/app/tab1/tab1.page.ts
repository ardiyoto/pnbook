import { Component } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { AlertService } from '../services/alert.service';
import { CONTACT, ContactService } from '../services/contact.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page {

  img: any = "assets/images/user.png"
  contacts: any

  constructor(
    public fS: AngularFirestore,
    public ct: ContactService,
    public al: AlertService
  ) {

  }

  ngOnInit() {
    this.ct.get_Contacts().subscribe((res) => {
      this.contacts = res.map((t) => {
        return {
          id: t.payload.doc.id,
          ...t.payload.doc.data() as CONTACT
        };
      })

      console.log(this.contacts)
    });
  }

  async delContact(id) {
    this.al.show_loading();
    await this.ct.delete_Contact(id).then(
      () => {
        this.al.notif("Contact Deleted !", "success", 1000)
      }
    ).catch(
      (err) => {
        this.al.notif(err, "danger", 1000)
      }
    )
    this.al.hide_loading();
  }

  async setFav(id, fav) {

    let vFav = fav == 0 ? 1 : 0;

    await this.fS.collection("contacts").doc(id).update({
      "fav": vFav
    }).catch(
      (err) => {
        this.al.notif(err, "danger", 1000)
      }
    )
  }

}
