import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FcontactPageRoutingModule } from './fcontact-routing.module';

import { FcontactPage } from './fcontact.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FcontactPageRoutingModule
  ],
  declarations: [FcontactPage]
})
export class FcontactPageModule {}
