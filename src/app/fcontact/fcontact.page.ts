import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { CONTACT, ContactService } from '../services/contact.service';

@Component({
  selector: 'app-fcontact',
  templateUrl: './fcontact.page.html',
  styleUrls: ['./fcontact.page.scss'],
})
export class FcontactPage implements OnInit {

  contact: CONTACT = {
    $key: null,
    avatar: null,
    email: null,
    fav: 0,
    name: null,
    phone: null,
  }
  id: any
  avatar: any
  mode: any = "ADD NEW"

  constructor(
    public ct: ContactService,
    public router: Router,
    public ac: ActivatedRoute,
    public al: AlertService
  ) {
    this.id = this.ac.snapshot.paramMap.get("id")
    this.avatar = this.contact.avatar ? this.contact.avatar : "assets/images/user.png"
    if (this.id) {
      this.mode = "EDIT"
      this.getContact()
    }
  }

  ngOnInit() {
  }

  async getContact() {
    await this.ct.get_Contact(this.id).subscribe(
      (data) => {
        this.contact.name = data["name"]
        this.contact.phone = data["phone"]
        this.contact.email = data["email"]
        this.contact.fav = data["fav"]
        this.contact.avatar = data["avatar"]
      }
    )
  }

  async onSubmit() {
    // Validation
    if (!this.contact.name) {
      this.al.notif("Name can't empty !", "warning", 1000)
      return
    }
    if (!this.contact.phone) {
      this.al.notif("Phone Number can't empty !", "warning", 1000)
      return
    }


    // Show Loading
    this.al.show_loading()

    // Condition for Add and Edit
    if (!this.id) {
      await this.ct.add_Contact(this.contact).then(
        () => {
          this.al.notif("New Contact Added", "success", 1500) // Notification when success Add New Contact
          this.router.navigate(['tabs/tab1'])
        }
      ).catch(
        (err) => {
          this.al.notif(err, "warning", 1000) // Notif When any Error
          console.log(err)
        }
      )
    } else {
      await this.ct.update_Contact(this.id, this.contact).then(
        () => {
          this.al.notif("New Contact Added", "success", 1500) // Notification when success Edit Contact
          this.router.navigate(['tabs/tab1'])
        }
      ).catch(
        (err) => {
          this.al.notif(err, "warning", 1000) // Notif When any Error
          console.log(err)
        }
      )
    }

    this.al.hide_loading() // Hide Loading

    // End Edit and Add 

  }

}
