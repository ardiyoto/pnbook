import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FcontactPage } from './fcontact.page';

const routes: Routes = [
  {
    path: '',
    component: FcontactPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FcontactPageRoutingModule {}
